#pragma once

#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>

#define THROW_EXCEPTION(X) std::throw_with_nested(Exception(__FILE__, __LINE__, X))

class Exception : public std::runtime_error {
private:
	std::string m_Msg;
public:
	Exception(const char *file, int line, const std::string &arg);

	const char* what() const noexcept override;
};