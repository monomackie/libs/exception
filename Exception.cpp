#include "Exception.h"

Exception::Exception(const char *file, int line, const std::string &arg) : std::runtime_error(arg) {
	m_Msg = std::string(file) + ":" + std::to_string(line) + ": " + arg;
}

const char *Exception::what() const noexcept  {
	return m_Msg.c_str();
}
